package com.pe.pichincha.tipocambio.controller;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pe.pichincha.tipocambio.modelo.TipoCambio;
import com.pe.pichincha.tipocambio.modelo.dto.request.TipoCambioRequest;
import com.pe.pichincha.tipocambio.modelo.dto.response.TipoCambioResponse;
import com.pe.pichincha.tipocambio.service.TipoCambioService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/tipocambio")
public class TipoCambioController {
	@Autowired
	private TipoCambioService service;

	
	ModelMapper model = new ModelMapper();

	@GetMapping
	public Mono<ResponseEntity<Flux<TipoCambioResponse>>> findAll() {
		Flux<TipoCambioResponse> fx = service.findAll(); // Flux<Client>
		return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(fx));
	}

	@GetMapping("/{id}")
	public Mono<ResponseEntity<TipoCambioResponse>> findById(@PathVariable("id") String id) {
		return service.findById(Long.parseLong(id)) // Mono<Client>
				.map(p -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(p));
	}

	@PostMapping
	public Mono<ResponseEntity<TipoCambioResponse>> save(@Valid @RequestBody TipoCambioRequest request
			) {
		TipoCambio tipoCambio = model.map(request, TipoCambio.class);
		return service.save(tipoCambio) // Mono<Client>
				.map(p -> ResponseEntity
						.ok()
						.contentType(MediaType.APPLICATION_JSON).body(p));
	}
	
	@PutMapping("/{id}")
    public Mono<ResponseEntity<TipoCambioResponse>> update(@Valid @PathVariable("id") String id, @RequestBody TipoCambioRequest client){

        Mono<TipoCambioRequest> mono = Mono.just(client);
        TipoCambioResponse response = model.map(mono, TipoCambioResponse.class);
        Mono<TipoCambioResponse> monoBody = Mono.just(response);
        Mono<TipoCambioResponse> monoBD = service.findById(Long.parseLong(id));

        return monoBD.zipWith(monoBody, (bd, cl) -> {
                    bd.setTipoCambioId(Long.parseLong(id));
                    bd.setMonedaDestino(cl.getMonedaDestino());
                    bd.setMonedaOrigen(cl.getMonedaOrigen());
                    bd.setMonto(cl.getMonto());
                    bd.setMontoCambioTotal(cl.getMonto().multiply(cl.getMontoTipoCambio()));
                    bd.setMontoTipoCambio(cl.getMontoTipoCambio());
                    bd.setTipoCambioMoneda(cl.getTipoCambioMoneda());
                    
                    return Mono.just(bd);
                })
                .flatMap(p -> {
                	TipoCambio requestService =  model.map(p, TipoCambio.class);
                	 return service.update(id,requestService);
                })
                .map(p -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
	
	@GetMapping("/buscar/{tipocambio}")
	public Mono<ResponseEntity<Flux<TipoCambioResponse>>> findByTipoCambio(@PathVariable("tipocambio") String tipocambio) {
		Flux<TipoCambioResponse> fx = service.findByTipoCambio(tipocambio);
		return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(fx));
	}

   

}
