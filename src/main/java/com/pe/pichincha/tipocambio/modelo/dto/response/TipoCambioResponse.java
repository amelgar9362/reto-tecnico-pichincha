package com.pe.pichincha.tipocambio.modelo.dto.response;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TipoCambioResponse {
	private Long tipoCambioId;
	private BigDecimal monto;
	private String monedaOrigen;
	private String monedaDestino;
	private BigDecimal montoTipoCambio;
	private String tipoCambioMoneda;
	private BigDecimal montoCambioTotal;

}
