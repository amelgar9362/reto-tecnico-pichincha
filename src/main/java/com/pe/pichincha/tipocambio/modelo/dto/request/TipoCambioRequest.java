package com.pe.pichincha.tipocambio.modelo.dto.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TipoCambioRequest {
	private Long tipoCambioId;
	@NotNull
	private BigDecimal monto;
	@NotEmpty
	private String monedaOrigen;
	@NotEmpty
	private String monedaDestino;
	@NotNull
	private BigDecimal montoTipoCambio;
	@NotEmpty
	private String tipoCambioMoneda;	
	private BigDecimal montoCambioTotal;
}
