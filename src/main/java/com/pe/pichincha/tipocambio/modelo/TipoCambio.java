package com.pe.pichincha.tipocambio.modelo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tipocambio")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TipoCambio {
	@Id
	@Column(name = "tipo_cambio_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long tipoCambioId;
	
	@Column(name="monto")
	private BigDecimal monto;
	
	@Column(name="moneda_origen")
	private String monedaOrigen;
	
	@Column(name="moneda_destino")
	private String monedaDestino;
	
	@Column(name="monto_tipo_cambio")
	private BigDecimal montoTipoCambio;
	
	@Column(name="tipo_cambio_moneda")
	private String tipoCambioMoneda;
	
	@Column(name="monto_cambio_total")
	private BigDecimal montoCambioTotal;

}
