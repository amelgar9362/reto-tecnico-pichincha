package com.pe.pichincha.tipocambio.service.impl;

import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.pe.pichincha.tipocambio.config.Moneda;
import com.pe.pichincha.tipocambio.dao.TipoCambioDao;
import com.pe.pichincha.tipocambio.modelo.TipoCambio;
import com.pe.pichincha.tipocambio.modelo.dto.response.TipoCambioResponse;
import com.pe.pichincha.tipocambio.service.TipoCambioService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TipoCambioServiceImpl implements TipoCambioService {

	@Autowired
	TipoCambioDao tipoCambioDao;

	ModelMapper model = new ModelMapper();

	@Override
	public Mono<TipoCambioResponse> save(TipoCambio tipoCambio) {
		if (tipoCambio.getTipoCambioMoneda().equals(Moneda.SOLES.getDescripcion())) {
			tipoCambio.setMontoCambioTotal(tipoCambio.getMonto().multiply(tipoCambio.getMontoTipoCambio()));
		}else {
			tipoCambio.setMontoCambioTotal(tipoCambio.getMonto().divide(tipoCambio.getMontoTipoCambio(),2, RoundingMode.HALF_UP));
		}
		TipoCambio responseDao = tipoCambioDao.save(tipoCambio);
		TipoCambioResponse responseTipoCambio = model.map(responseDao, TipoCambioResponse.class);
		return Mono.just(responseTipoCambio);
	}

	@Override
	public Mono<TipoCambioResponse> update(String id, TipoCambio tipoCambio) {
		Optional<TipoCambio> responseDao = tipoCambioDao.findById(Long.parseLong(id));
		TipoCambioResponse responseTipoCambio = null;
		if (responseDao.isPresent()) {
			TipoCambio response = responseDao.get();
			if (tipoCambio.getTipoCambioMoneda().equals(Moneda.SOLES.getDescripcion())) {
				tipoCambio.setMontoCambioTotal(tipoCambio.getMonto().multiply(tipoCambio.getMontoTipoCambio()));
			}else {
				tipoCambio.setMontoCambioTotal(tipoCambio.getMonto().divide(tipoCambio.getMontoTipoCambio(),2, RoundingMode.HALF_UP));
			}
			response = tipoCambioDao.save(tipoCambio);
			responseTipoCambio = model.map(response, TipoCambioResponse.class);
		}
		return Mono.just(responseTipoCambio);
	}

	@Override
	public Flux<TipoCambioResponse> findAll() {
		List<TipoCambio> listResponse = tipoCambioDao.findAll();
		List<TipoCambioResponse> responseTipoCambio = model.map(listResponse,
				new TypeToken<List<TipoCambioResponse>>() {
				}.getType());
		return Flux.fromIterable(responseTipoCambio);
	}

	@Override
	public Mono<TipoCambioResponse> findById(Long id) {
		Optional<TipoCambio> responseDao = tipoCambioDao.findById(id);
		TipoCambioResponse responseTipoCambio = null;
		if (responseDao.isPresent()) {
			responseTipoCambio = model.map(responseDao.get(), TipoCambioResponse.class);
		}
		return Mono.just(responseTipoCambio);
	}

	@Override
	public Flux<TipoCambioResponse> findByTipoCambio(String tipoCambio) {
		List<TipoCambioResponse> responseTipoCambio = null;
		TipoCambio request = new TipoCambio();
		request.setTipoCambioMoneda(tipoCambio);
		Example<TipoCambio> exampleOf = Example.of(request);
		List<TipoCambio> responseDao = tipoCambioDao.findAll(exampleOf);
		if (!responseDao.isEmpty()) {
			responseTipoCambio = model.map(responseDao, new TypeToken<List<TipoCambioResponse>>() {
			}.getType());
		}
		return Flux.fromIterable(responseTipoCambio);
	}

}
