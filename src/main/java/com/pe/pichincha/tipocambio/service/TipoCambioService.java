package com.pe.pichincha.tipocambio.service;

import com.pe.pichincha.tipocambio.modelo.TipoCambio;
import com.pe.pichincha.tipocambio.modelo.dto.response.TipoCambioResponse;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TipoCambioService {
	Mono<TipoCambioResponse> save(TipoCambio tipoCambio); 
	Mono<TipoCambioResponse> update(String id,TipoCambio tipoCambio) ;
	Flux<TipoCambioResponse> findAll();
	Mono<TipoCambioResponse> findById(Long id);
	Flux<TipoCambioResponse> findByTipoCambio(String tipoCambio);
	

}
