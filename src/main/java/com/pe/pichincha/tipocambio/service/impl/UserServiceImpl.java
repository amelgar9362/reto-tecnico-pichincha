package com.pe.pichincha.tipocambio.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.pe.pichincha.tipocambio.dao.IGenericRepo;
import com.pe.pichincha.tipocambio.dao.IRoleRepo;
import com.pe.pichincha.tipocambio.dao.IUserRepo;
import com.pe.pichincha.tipocambio.modelo.User;
import com.pe.pichincha.tipocambio.service.IUserService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl extends CRUDImpl<com.pe.pichincha.tipocambio.modelo.User, String> implements IUserService {

	@Autowired
	private IUserRepo repo;
	
	@Autowired
	private IRoleRepo rolRepo;
	
	@Autowired
	private BCryptPasswordEncoder bcrypt;
	
	@Override
	protected IGenericRepo<User, String> getRepo() {
		return repo; 
	}
	
	@Override
	public Mono<com.pe.pichincha.tipocambio.security.User> buscarPorUsuario(String username) {
		com.pe.pichincha.tipocambio.modelo.User usuario = repo.findOneByUsername(username);
		Mono<com.pe.pichincha.tipocambio.modelo.User> monoUsuario = Mono.just(usuario);
		List<String> roles = new ArrayList<String>();
		
		return monoUsuario.flatMap(u -> {
			return Flux.fromIterable(u.getRoles())
					.flatMap(rol -> {
						return Mono.just(rolRepo.findById(rol.getId()).get()) 
								.map(r -> {
									roles.add(r.getName());
									return r;
								});
					}).collectList().flatMap(list -> {
						u.setRoles(list);
						return Mono.just(u);
					});
		})	
		.flatMap(u -> {			
			return Mono.just(new com.pe.pichincha.tipocambio.security.User(u.getUsername(), u.getPassword(), u.getStatus(), roles));
		});
	}

	@Override
	public Mono<User> registrarHash(User user) {
		user.setPassword(bcrypt.encode(user.getPassword()));
		return Mono.just(repo.save(user)) ;
	}

	



}
