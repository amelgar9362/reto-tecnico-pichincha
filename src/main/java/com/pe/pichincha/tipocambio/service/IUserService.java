package com.pe.pichincha.tipocambio.service;



import com.pe.pichincha.tipocambio.modelo.User;

import reactor.core.publisher.Mono;

public interface IUserService extends ICRUD<User, String>{

	Mono<User> registrarHash(User usuario);
	Mono<com.pe.pichincha.tipocambio.security.User> buscarPorUsuario(String usuario);
}
