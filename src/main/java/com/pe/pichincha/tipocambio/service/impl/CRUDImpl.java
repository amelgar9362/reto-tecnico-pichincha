package com.pe.pichincha.tipocambio.service.impl;

import com.pe.pichincha.tipocambio.dao.IGenericRepo;
import com.pe.pichincha.tipocambio.service.ICRUD;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public abstract class CRUDImpl<T, ID> implements ICRUD<T, ID> {

    protected abstract IGenericRepo<T, ID> getRepo();
    @Override
    public Mono<T> save(T t) {
        return Mono.just(getRepo().save(t)) ;
    }

    @Override
    public Mono<T> update(T t) {
        return Mono.just(getRepo().save(t));
    }

    @Override
    public Flux<T> findAll() {
        return Flux.fromIterable(getRepo().findAll());
    }

    @Override
    public Mono<T> findById(ID id) {
        return Mono.just(getRepo().findById(id).get());
    }

//    @Override
//    public Mono<Void> delete(ID id) {
//        return Mono.just(getRepo().deleteById(id));
//    }

//    @Override
//    public Mono<PageSupport<T>> getPage(Pageable page){
//        return getRepo().findAll() //Flux<T>
//                .collectList() //Mono<List<T>>
//                .map(list -> new PageSupport<>(
//                        list.stream()
//                                .skip(page.getPageNumber() * page.getPageSize())
//                                .limit(page.getPageSize())
//                                .collect(Collectors.toList()),
//                        page.getPageNumber(), page.getPageSize(), list.size())
//                );
//    }

}
