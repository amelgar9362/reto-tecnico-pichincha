package com.pe.pichincha.tipocambio.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperInitializer {
	
	public ModelMapper inicializacion () {
		return new ModelMapper();
	}

}
