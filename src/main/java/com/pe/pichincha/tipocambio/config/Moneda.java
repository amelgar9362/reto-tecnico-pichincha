package com.pe.pichincha.tipocambio.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Moneda {
	SOLES("PEN"),
	DOLAR("USD"),
	EURO("EUR");
	
	private String descripcion;

}
