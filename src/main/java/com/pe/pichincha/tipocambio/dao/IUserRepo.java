package com.pe.pichincha.tipocambio.dao;

import com.pe.pichincha.tipocambio.modelo.User;

public interface IUserRepo extends IGenericRepo<User, String>{

	//SELECT * FROM USER U WHERE U.USERNAME = ?
	//{username : ?}
	//DerivedQueries
	User findOneByUsername(String username);
}
