package com.pe.pichincha.tipocambio.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.pe.pichincha.tipocambio.modelo.TipoCambio;

public interface TipoCambioDao extends JpaRepository<TipoCambio, Long> {

}
