package com.pe.pichincha.tipocambio.dao;

import com.pe.pichincha.tipocambio.modelo.Role;

public interface IRoleRepo extends IGenericRepo<Role, String>{

}
